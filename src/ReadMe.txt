# Getting started
## Installation
Install all the dependencies using composer
    composer install
Start the local development server
    php artisan serve
Dependencies:
- Laravel/framework: "^7.29"
- PHP: "^7.2.5|^8.0" 
You can now access the server at http://localhost:8000