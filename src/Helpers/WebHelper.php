<?php

namespace WerdenIt\TokkoIntegrationPackage\src;

class WebHelper{
    public static function normalizeToUrl($str)
    {
        $str = str_replace('Á', 'a', $str);
        $str = str_replace('É', 'e', $str);
        $str = str_replace('Í', 'i', $str);
        $str = str_replace('Ó', 'o', $str);
        $str = str_replace('Ú', 'u', $str);

        $str = str_replace('á', 'a', $str);
        $str = str_replace('é', 'e', $str);
        $str = str_replace('í', 'i', $str);
        $str = str_replace('ó', 'o', $str);
        $str = str_replace('ú', 'u', $str);

        $str = str_replace('ü', 'u', $str);
        $str = str_replace('ñ', 'n', $str);

        return strtolower(str_replace(' ', '-', $str));
    }

    public static function propertyUrl($property, $operation = null){
        if(!$operation)
          $operation = $property->operations[0]->operation_type;

        $url = strtolower(str_replace(' ', '-', $operation));

        $url .= '-' . strtolower(str_replace(' ', '-', $property->type->name));

        if($property->room_amount)
          $url .= '-' . $property->room_amount . '-ambiente' . ($property->room_amount > 1 ? 's' : '');

        $url .= '-en-' . strtolower(str_replace(' ', '-', $property->location->name));

        $url = str_replace('Á', 'a', $url);
        $url = str_replace('É', 'e', $url);
        $url = str_replace('Í', 'i', $url);
        $url = str_replace('Ó', 'o', $url);
        $url = str_replace('Ú', 'u', $url);

        $url = str_replace('á', 'a', $url);
        $url = str_replace('é', 'e', $url);
        $url = str_replace('í', 'i', $url);
        $url = str_replace('ó', 'o', $url);
        $url = str_replace('ú', 'u', $url);

        $url = str_replace('ü', 'u', $url);
        $url = str_replace('ñ', 'n', $url);

        return $url;
      }

      public static function developmentUrl($development){
        $url = strtolower(str_replace(' ', '-', $development->type->name));

        $url .= '-en-' . strtolower(str_replace(' ', '-', $development->location->name));

        return $url;
      }

      public static function developmentConstructionDate($development){
          if($development->construction_date < date('Y-m-d')){
            return 'Entrega Inmediata';
          }else{
            return self::getMonth(date('m', strtotime($development->construction_date))) . ' ' . date('Y', strtotime($development->construction_date));
          }
      }

      public static function developmentConstructionStatus($development){
        switch($development->construction_status){
            case 1:
            default:
                $status = 'Desconocido';
                break;
            case 2:
                $status = 'Reuniendo Inversores';
                break;
            case 3:
                $status = 'En Pozo';
                break;
            case 4:
                $status = 'En Construcción';
                break;
            case 5:
                $status = 'Construcción Detenida';
                break;
            case 6:
                $status = 'Construcción Terminada';
                break;
        }

        return $status;
      }

      public static function propertySimilarFilter($property, $operation = null){
        if(!$operation)
          $operation = $property->operations[0]->operation_type;

        foreach($property->operations as $o){
          if($o->operation_type == $operation)
            $price = $o->prices[0];
        }

        $tokkoFilter = ['filters' => []];


        if(strtolower($operation) == 'venta'){
          $price_from = $price->price - 20000;
          $price_to = $price->price * 30 / 100;

          $tokkoFilter['operation_types'] = [config('tokko.operations.venta')];

          $tokkoFilter['filters'][] = ['room_amount', '=', $property->room_amount];

          $tokkoFilter['property_types'] = [$property->type->id];
        }elseif(strtolower($operation) == 'alquiler'){
          $price_from = $price->price - 5000;
          $price_to = $price->price * 30 / 100;

          $tokkoFilter['operation_types'] = [config('tokko.operations.alquiler')];

          $tokkoFilter['filters'][] = ['room_amount', '=', $property->room_amount];

          $tokkoFilter['property_types'] = [$property->type->id];
        }else{
          $price_from = $price->price - 5000;
          $price_to = $price->price * 30 / 100;

          $tokkoFilter['operation_types'] = [config('tokko.operations.alquiler_temporario')];

          $tokkoFilter['filters'][] = ['custom1', '>', ($property->custom1 - 1)];

        }

        $tokkoFilter['currency'] = $price->currency;

        if(!$property->development){
            $tokkoFilter['price_from'] = $price_from;
            $tokkoFilter['price_to'] = $price_to;
        }else{

        }


    //    $tokkoFilter['current_localization_type'] = 'division';
      //  $tokkoFilter['current_localization_id'] = $property->location->id;

        return $tokkoFilter;
      }

      public static function parseUrl($url){
        $tokkoFilter = ['filters' => []];

        //prices
        $regexp = "/precio-[(0-9)]+-a-[(0-9)]+-[(A-Z)]+/";
        preg_match_all($regexp, $url, $matches);
        if(count($matches[0])>0)
        {
          $prices = explode('-', $matches[0][0]);
          
          $tokkoFilter['currency'] = $prices[4];
          $tokkoFilter['price_from'] = $prices[1];
          $tokkoFilter['price_to'] = $prices[3];

          $url = str_replace($matches[0][0], '', $url);
        }

        //codigo
        $regexp = "/por-codigo-[(a-z)(A-Z)(0-9)]+/";
        preg_match_all($regexp, $url, $matches);
        if(count($matches[0])>0)
        {
          $codes = explode('-', $matches[0][0]);

          $code = $codes[2];

          //$tokkoFilter['external']['reference_code__in'] = $code;

          $tokkoFilter['filters'][] = ["reference_code", "contains", $code];

              $url = str_replace($matches[0][0], '', $url);
        }

        self::roomFilter($tokkoFilter, $url, 'banios', 'bathroom_amount');
        self::roomFilter($tokkoFilter, $url, 'ambientes', 'room_amount');
        self::roomFilter($tokkoFilter, $url, 'huespedes', 'custom1', true);
        self::roomFilter($tokkoFilter, $url, 'dormitorios', 'suite_amount');
        self::roomFilter($tokkoFilter, $url, 'metros', 'total_surface');

        $regexp = "/ubicacion-[(0-9)]+-[(a-zA-Z\-0-9)]+/";
        preg_match_all($regexp, $url, $matches);
        if(count($matches[0])>0)
        {
        $locations = explode('-', $matches[0][0]);

        $location_id = $locations[1];
        $location_name = str_replace($locations[0].'-'.$locations[1].'-', '', $matches[0][0]);

        $tokkoFilter['current_localization_type'] = 'division';
        $tokkoFilter['current_localization_name'] = str_replace('-', ' ', $location_name);
        $tokkoFilter['current_localization_id'] = $location_id;

            $url = str_replace($matches[0][0], '', $url);
        }

        $regexp = "/ingreso-[(0-9)]+-[(0-9)]+-[(0-9)]+-mes/";
        preg_match_all($regexp, $url, $matches);
        if(count($matches[0])>0)
        {
          $availabilities = explode('-', $matches[0][0]);

          $year_month = $availabilities[2].'-'.$availabilities[1];

          if($year_month == date('Y-m'))
            $day = date('d');
          else
            $day = '01';

          $date_from = date('Y-m-d', strtotime($year_month.'-'.$day));
          $months = $availabilities[3];

          $date_to = date('Y-m-d', strtotime('+'.$months.' month', strtotime($date_from)));

          $tokkoFilter['availability'] = [$date_from, $date_to];

              $url = str_replace($matches[0][0], '', $url);
        }

        if(strlen($url) == 0)
            return $tokkoFilter;

        $dictonary = self::filterDictonary();

        foreach($dictonary as $pattern => $filter){
          $regexp = "/".$pattern."/";
          preg_match_all($regexp, $url, $matches);
          if(count($matches[0])>0)
          {
            if($filter['filters']){
              if(isset($tokkoFilter['filters'][$filter['name']]))
                $tokkoFilter['filters'][$filter['name']][] = $filter['value'][0];
              else
                $tokkoFilter['filters'][] = [$filter['name'], $filter['value']];
            }else{
            //   if(is_array($tokkoFilter[$filter['name']]))
            //     $tokkoFilter[$filter['name']][] = $filter['value'][0];
            //   else
                $tokkoFilter[$filter['name']] = $filter['value'];
            }

            $url = str_replace($pattern, '', $url);
          }

          if(strlen($url) == 0)
            break;
        }

        return $tokkoFilter;
      }

      public static function roomFilter(&$tokkoFilter, $url, $pattern, $type, $grater_that = false){
        $regexp = "/de-[(0-9)]+-a-[(0-9)]+-".$pattern."-o-mas/";
        preg_match_all($regexp, $url, $matches);
          if(count($matches[0])>0)
          {
          $room_amounts = explode('-', $matches[0][0]);

          $room_amount_from = (int)$room_amounts[1] - 1;
          $tokkoFilter['filters'][] = [$type, '>', $room_amount_from];

          $url = str_replace($matches[0][0], '', $url);
        }

        $regexp = "/de-[(0-9)]+-a-[(0-9)]+-".$pattern."/";
        preg_match_all($regexp, $url, $matches);
          if(count($matches[0])>0)
          {
          $room_amounts = explode('-', $matches[0][0]);

          $room_amount_from = (int)$room_amounts[1] - 1;
          $tokkoFilter['filters'][] = [$type, '>', $room_amount_from];

          $room_amount_to = (int)$room_amounts[3] + 1;
          $tokkoFilter['filters'][] = [$type, '<', $room_amount_to];

          $url = str_replace($matches[0][0], '', $url);
        }

        $regexp = "/[(0-9)]+-".$pattern."-o-mas/";
        preg_match_all($regexp, $url, $matches);
          if(count($matches[0])>0)
          {
          $room_amounts = explode('-', $matches[0][0]);

          $room_amount_from = (int)$room_amounts[0] - 1;
          $tokkoFilter['filters'][] = [$type, '>', $room_amount_from];

          $url = str_replace($matches[0][0], '', $url);
        }

        $regexp = "/[(0-9)]+-".$pattern."/";
        preg_match_all($regexp, $url, $matches);
        if(count($matches[0])>0)
        {
          $room_amounts = explode('-', $matches[0][0]);

          if($grater_that){
            $room_amount_from = (int)$room_amounts[0] - 1;
            $tokkoFilter['filters'][] = [$type, '>', $room_amount_from];
          }else{
            $tokkoFilter['filters'][] = [$type, '=', $room_amounts[0]];
          }

              $url = str_replace($matches[0][0], '', $url);
        }
      }

      public static function frontCoverPhoto($property){
        foreach($property->photos as $photo){
          if($photo->is_front_cover)
            $frontCover = $photo->image;
        }

        if(!isset($frontCover)){
          if(count($property->photos) > 0)
            $frontCover = $property->photos[0]->image;
          else
            $frontCover = url('assets/img/logogazda.jpg');
        }

        return $frontCover;
      }

      public static function buildSearchTitle(&$filters){
            $applied = [];

            if(isset($filters['property_types']) && count($filters['property_types']) == 1){
                switch($filters['property_types'][0]){
                    case 1:
                        $title = 'Terreno';
                        break;
                    case 2:
                        $title = 'Departamento';
                        break;
                    case 3:
                        $title = 'Casa';
                        break;
                    case 4:
                        $title = 'Quinta';
                        break;
                    case 5:
                        $title = 'Oficina';
                        break;
                    case 6:
                        $title = 'Amarra';
                        break;
                    case 7:
                        $title = 'Local';
                        break;
                    case 8:
                        $title = 'Edificio Comercial';
                        break;
                    case 9:
                        $title = 'Campo';
                        break;
                    case 10:
                        $title = 'Cochera';
                        break;
                    case 11:
                        $title = 'Hotel';
                        break;
                    case 12:
                        $title = 'Nave Industrial';
                        break;
                    case 13:
                        $title = 'PH';
                        break;
                    case 14:
                        $title = 'Depósito';
                        break;
                    case 15:
                        $title = 'Fondo de Comercio';
                        break;
                    case 16:
                        $title = 'Baulera';
                        break;
                    case 17:
                        $title = 'Bodega';
                        break;
                    case 18:
                        $title = 'Finca';
                        break;
                    case 19:
                        $title = 'Chacra';
                        break;
                    case 20:
                        $title = 'Cama Naútica';
                        break;
                    case 21:
                        $title = 'Isla';
                        break;
                    case 22:
                        $title = 'Terraza';
                        break;
                    case 23:
                        $title = 'Galpón';
                        break;
                    case 25:
                        $title = 'Villa';
                        break;
                }

                $applied['property_type'] = ['value' => str_replace(' ', '-', strtolower($title)), 'label' => $title];
            }else{
                $title = 'Propiedades';
            }

            if(isset($filters['operation_types']) && count($filters['operation_types']) == 1){
                switch($filters['operation_types'][0]){
                    case 1:
                        $title .= ' en Venta';
                        $applied['operation_type'] = ['value' => 'en-venta', 'label' => 'Venta'];
                        break;
                    case 2:
                        $title .= ' en Alquiler';
                        $applied['operation_type'] = ['value' => 'en-alquiler', 'label' => 'Alquiler'];
                        break;
                    case 3:
                        $title .= ' en Alquiler temporario';
                        $applied['operation_type'] = ['value' => 'en-alquiler-temporario', 'label' => 'Alquiler Temporario'];
                        break;
                }
            }

            if(isset($filters['filters']) && isset($filters['filters'])){
                foreach($filters['filters'] as $filter){
                    if($filter[0] == 'room_amount'){
                        $title .= ' de ' . $filter[2] . ' ambiente' . ($filter[2] == 1 ? '' : 's');

                        $applied['room_amount'] = ['value' => ($filter[2].'-ambientes'), 'label' => $filter[2] . ' Ambiente' . ($filter[2] == 1 ? '' : 's')];

                        break;
                    }
                }

                foreach($filters['filters'] as $k=>$filter){
                    if($filter[0] == 'roofed_surface' && $filter[1] == '='){
                        $title .= ' de ' . $filter[2] . ' ambiente' . ($filter[2] == 1 ? '' : 's');

                        $applied['roofed_surface'] = ['value' => ($filter[2].'-metros'), 'label' => $filter[2] . ' m<sup>2</sup>'];

                        break;
                    }elseif($filter[0] == 'roofed_surface' && ($filter[1] == '<' || $filter[1] == '>')){
                        $filter1 = $filter;
                        $filter2 = $filters['filters'][$k + 1];

                        if($filter1[1] == '>'){
                            $applied['roofed_surface'] = ['value' => ('de-'.$filter1[2].'-a-'.$filter2[2].'-metros'), 'label' => 'de ' . $filter1[2] . 'm<sup>2</sup> a ' . $filter2[2] . ' m<sup>2</sup>'];
                        }else{
                            $applied['roofed_surface'] = ['value' => ('de-'.$filter2[2].'-a-'.$filter1[2].'-metros'), 'label' => 'de ' . $filter2[2] . 'm<sup>2</sup> a ' . $filter1[2] . ' m<sup>2</sup>'];
                        }

                        break;
                    }
                }

                foreach($filters['filters'] as $k=>$filter){
                    if($filter[0] == 'roofed_surface' && $filter[1] == '='){
                        $title .= ' de ' . $filter[2] . ' ambiente' . ($filter[2] == 1 ? '' : 's');

                        $applied['room_amount'] = ['value' => ($filter[2].'-metros'), 'label' => $filter[2] . ' m<sup>2</sup>'];

                        break;
                    }elseif($filter[0] == 'roofed_surface' && ($filter[1] == '<' || $filter[1] == '>')){
                        $filter1 = $filter;
                        $filter2 = $filters['filters'][$k + 1];

                        if($filter1[1] == '>'){
                            $applied['roofed_surface'] = ['value' => ('de-'.$filter1[2].'-a-'.$filter2[2].'-metros'), 'label' => 'de ' . $filter1[2] . 'm<sup>2</sup> a ' . $filter2[2] . ' m<sup>2</sup>'];
                        }else{
                            $applied['roofed_surface'] = ['value' => ('de-'.$filter2[2].'-a-'.$filter1[2].'-metros'), 'label' => 'de ' . $filter2[2] . 'm<sup>2</sup> a ' . $filter1[2] . ' m<sup>2</sup>'];
                        }

                        break;
                    }
                }

                foreach($filters['filters'] as $filter){
                    if($filter[0] == 'bathroom_amount'){
                        $applied['bathroom_amount'] = ['value' => ($filter[2].'-banios'), 'label' => $filter[2] . ' Baño' . ($filter[2] == 1 ? '' : 's')];

                        break;
                    }
                }

                if(isset($filters['current_localization_name'])){
                    $value = 'ubicacion-' . $filters['current_localization_id'] . '-' . str_replace(' ', '-', $filters['current_localization_name']);
                    $applied['location'] = ['value' => $value, 'label' => ucfirst($filters['current_localization_name'])];

                    $title .= ' en ' . ucfirst($filters['current_localization_name']);
                }

                if(isset($filters['price_from']) && $filters['price_from'] != 0){
                    $applied['price_from'] = ['value' => $filters['price_from'], 'label' => 'Desde ' . number_format($filters['price_from'], 0, ',', '.')];
                }
                if(isset($filters['price_to']) && $filters['price_to'] != 999999999999){
                    $applied['price_to'] = ['value' => $filters['price_to'], 'label' => 'Hasta ' . number_format($filters['price_to'], 0, ',', '.')];
                }
            }

            $filters['title'] = $title;
            $filters['applied'] = $applied;
        }

      private static function filterDictonary(){
          return [
            //operation types
            'en-venta' => [
              'name' => 'operation_types',
              'value' => [1],
              'filters' => false
            ],
            'en-alquiler-temporario' => [
              'name' => 'operation_types',
              'value' => [3],
              'filters' => false
            ],
            'en-alquiler' => [
              'name' => 'operation_types',
              'value' => [2],
              'filters' => false
            ],

            //property types
            'terreno' => [
              'name' => 'property_types',
              'value' => [1],
              'filters' => false
            ],
            'departamento' => [
              'name' => 'property_types',
              'value' => [2],
              'filters' => false
            ],
            'casa' => [
              'name' => 'property_types',
              'value' => [3],
              'filters' => false
            ],
            'quienta' => [
              'name' => 'property_types',
              'value' => [4],
              'filters' => false
            ],
            'oficina' => [
              'name' => 'property_types',
              'value' => [5],
              'filters' => false
            ],
            'amarra' => [
              'name' => 'property_types',
              'value' => [6],
              'filters' => false
            ],
            'local' => [
              'name' => 'property_types',
              'value' => [7],
              'filters' => false
            ],
            'edificio-comercial' => [
              'name' => 'property_types',
              'value' => [8],
              'filters' => false
            ],
            'campo' => [
              'name' => 'property_types',
              'value' => [9],
              'filters' => false
            ],
            'cochera' => [
              'name' => 'property_type',
              'value' => [10],
              'filters' => false
            ],
            'hotel' => [
              'name' => 'property_types',
              'value' => [11],
              'filters' => false
            ],
            'nave-industrial' => [
              'name' => 'property_types',
              'value' => [12],
              'filters' => false
            ],
            'ph' => [
              'name' => 'property_types',
              'value' => [13],
              'filters' => false
            ],
            'deposito' => [
              'name' => 'property_types',
              'value' => [14],
              'filters' => false
            ],
            'fondo-de-comercio' => [
              'name' => 'property_types',
              'value' => [15],
              'filters' => false
            ],
            'baulera' => [
              'name' => 'property_types',
              'value' => [16],
              'filters' => false
            ],
            'bodega' => [
              'name' => 'property_types',
              'value' => [17],
              'filters' => false
            ],
            'finca' => [
              'name' => 'property_types',
              'value' => [18],
              'filters' => false
            ],
            'chacra' => [
              'name' => 'property_types',
              'value' => [19],
              'filters' => false
            ],
            'cama-nautica' => [
              'name' => 'property_types',
              'value' => [20],
              'filters' => false
            ],
            'isla' => [
              'name' => 'property_types',
              'value' => [21],
              'filters' => false
            ],
            'terraza' => [
              'name' => 'property_types',
              'value' => [22],
              'filters' => false
            ],
            'galpon' => [
              'name' => 'property_types',
              'value' => [23],
              'filters' => false
            ],
            'villa' => [
              'name' => 'property_types',
              'value' => [25],
              'filters' => false
            ],
            'piscina' => [
              'name' => 'with_tags',
              'value' => [51],
              'filters' => false
            ],
            'seguridad' => [
              'name' => 'with_tags',
              'value' => [42],
              'filters' => false
            ],
            'pet-friendly' => [
              'name' => 'with_tags',
              'value' => [46],
              'filters' => false
            ],
            'laundry' => [
              'name' => 'with_tags',
              'value' => [20],
              'filters' => false
            ],
            'balcon' => [
              'name' => 'with_tags',
              'value' => [10],
              'filters' => false
            ],
            'terraza' => [
              'name' => 'with_tags',
              'value' => [25],
              'filters' => false
            ],
            'patio' => [
              'name' => 'with_tags',
              'value' => [23],
              'filters' => false
            ],
            'reserva-sin-costo' => [
              'name' => 'with_custom_tags',
              'value' => [config('tokko.custom_tags.exclusivo')],
              'filters' => false
            ]
          ];
      }

    public static function getMonth($m){
        $_month = [
            '01' => 'Enero',
            '02' => 'Febrero',
            '03' => 'Marzo',
            '04' => 'Abril',
            '05' => 'Mayo',
            '06' => 'Junio',
            '07' => 'Julio',
            '08' => 'Agosto',
            '09' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre'
        ];

        return $_month[str_pad($m, 2, '0', STR_PAD_LEFT)];
    }
}
