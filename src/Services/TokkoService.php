<?php

namespace App\Http\Services;

use GuzzleHttp\Client;

class TokkoService extends Service
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => config('tokko.api_url'),
        ]);

        $this->api_key = config('tokko.api_key');
    }

    public function property($id){
        $method = 'v1/property/' . $id;
        $data = [
            'format' => 'json',
            'lang' => 'es_ar',
            'key' => $this->api_key
        ];

        $result = $this->get($method, $data);

        return $result;
    }

    public function development($id){
        $method = 'v1/development/' . $id;
        $data = [
            'format' => 'json',
            'lang' => 'es_ar',
            'key' => $this->api_key
        ];

        $result = $this->get($method, $data);

        return $result;
    }

    public function developmentProperties($developmentIds = []){
        $filters = [];
        $this->setMinimalFilters($filters);
        $filters['filters'][] = ['development__id', 'op', $developmentIds];

        return $this->searchProperties($filters, 99, 0);
    }

    public function searchProperties(&$filters, $limit = 20, $offset = 0){
        $this->setMinimalFilters($filters);

        $externalFilters = [];

        if(isset($filters['external'])){
            $externalFilters = $filters['external'];
            unset($filters['external']);
        }

        if(isset($externalFilters['reference_code__in']))
            $method = 'v1/property/';
        else
            $method = 'v1/property/search';

        $data = [
            'format' => 'json',
            'lang' => 'es_ar',
            'key' => $this->api_key,
            'limit' => $limit,
            'offset' => $offset,
            'order' => 'asc',
            'order_by' => 'price',
            'data' => json_encode($filters)
        ];

        $data = array_merge($data, $externalFilters);

        $result = $this->get($method, $data);

        return $result;
    }

    public function searchDevelopments($limit = 20, $offset = 0){
        $method = 'v1/development/';
        $data = [
            'format' => 'json',
            'lang' => 'es_ar',
            'key' => $this->api_key,
            'limit' => $limit,
            'offset' => $offset
        ];

        $result = $this->get($method, $data);

        return $result;
    }

    public function starredDevelopments($limit = 20, $offset = 0){
        $method = 'v1/development/';
        $data = [
            'format' => 'json',
            'lang' => 'es_ar',
            'key' => $this->api_key,
            'limit' => $limit,
            'offset' => $offset,
            'is_starred_on_web' => 'Yes'
        ];

        $result = $this->get($method, $data);

        return $result;
    }

    public function searchLocations($term){
        $method = 'v1/location/quicksearch';
        $data = [
            'format' => 'json',
            'lang' => 'es_ar',
            'key' => $this->api_key,
            'state' => '146,147,148,149',
            'q' => $term
        ];

        $result = $this->get($method, $data);

        return $result;
    }

    public function allSummary(){
        $this->setMinimalFilters($filters);

        $method = 'v1/property/get_search_summary';
        $data = [
            'format' => 'json',
            'lang' => 'es_ar',
            'key' => $this->api_key,
            'data' => json_encode($filters)
        ];

        $result = $this->get($method, $data);

        return $result;
    }

    private function setMinimalFilters(&$filters){
        if(is_object($filters))
            $filters = (array)$filters;

        if(!isset($filters['current_localization_id']) || strlen($filters['current_localization_id']) == 0)
            $filters['current_localization_id'] = config('tokko.default_filter_values.current_localization_id');

        if(!isset($filters['current_localization_type']) || strlen($filters['current_localization_type']) == 0)
            $filters['current_localization_type'] = config('tokko.default_filter_values.current_localization_type');

        if(!isset($filters['price_from']) || strlen($filters['price_from']) == 0)
            $filters['price_from'] = config('tokko.default_filter_values.price_from');

        if(!isset($filters['price_to']) || strlen($filters['price_to']) == 0)
            $filters['price_to'] = config('tokko.default_filter_values.price_to');

        if(!isset($filters['currency']) || strlen($filters['currency']) == 0)
            $filters['currency'] = config('tokko.default_filter_values.currency');

        if(!isset($filters['operation_types']) || !($filters['operation_types']))
            $filters['operation_types'] = config('tokko.default_filter_values.operation_types');

        if(!isset($filters['property_types']) || !($filters['property_types']))
            $filters['property_types'] = config('tokko.default_filter_values.property_types');

            if(!isset($filters['property_types']) || !($filters['property_types']))
            $filters['property_types'] = config('tokko.default_filter_values.property_types');

        if(!isset($filters['filters']))
            $filters['filters'] = [];
    }

    public function importProperties($limit = 20, $offset = 0)
    {
        $method = 'app/property/search';
        $data = [
            'format' => 'json',
            'lang' => 'es_ar',
            'key' => $this->api_key,
            'limit' => $limit,
            'offset' => $offset,
            'data' => '{"only_available":"undefined","only_reserved":"undefined","only_to_be_cotized":"undefined","only_not_available":"undefined", "current_localization_id":0, "current_localization_type": "country","price_from":0,"price_to":99999999999,"operation_types":[1,2,3],"property_types":[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20], "currency":"ANY", "filters" : []}'
        ];

        $result = $this->get($method, $data);

        return $result;
    }

    public function contact($form) : void
    {
        $method = 'v1/webcontact/';
        $data = [
            'key' => $this->api_key
        ];

        $this->client->post($method, [
            'json' => $form,
            'query' => $data
        ]);
    }

    public function updateLastRun() : void
    {
        $this->conf->last_run = date('Y-m-d H:i:s');
        $this->conf->save();
    }

    /**
     * Format date to requested by tokko api, eg: 2020-03-26T14:00:00.
     *
     * @param mixed $date the date to formate
     *
     * @return mixed the date formatted.
     */
    private function formatDate($date){
        return date('Y-m-d', strtotime($date)) . 'T' . date('H:i:s', strtotime($date));
    }
}
