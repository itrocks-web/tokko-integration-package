<?php

namespace App\Http\Services;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class Service
{
    public function __construct() {

    }

    protected function post($method, $data){
        try{
            $response = $this->client->post($method, [
                'json' => $data
            ]);

            $response = json_decode((string) $response->getBody());

            return $response;
        }catch(ClientException $e){
            $response = new \StdClass();

            $response->error = true;
            $response->message = $e->getMessage();

            return $response;
        }catch(ServerException $e){
            $response = json_decode((string)$e->getResponse()->getBody());

            $response->error = true;

            return $response;
        }
    }

    protected function get($method, $data){
        try{
            $response = $this->client->get($method, [
                'query' => $data
            ]);

            $response = json_decode((string) $response->getBody());

            return $response;
        }catch(ClientException $e){
            $response = new \StdClass();

            $response->error = true;
            $response->message = $e->getMessage();

            return $response;
        }catch(ServerException $e){
            $response = new \StdClass();

            $response->error = true;
            $response->message = json_decode((string)$e->getResponse()->getBody());

            return $response;
        }
    }
}