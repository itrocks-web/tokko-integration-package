<?php

return [
    'api_url' => 'http://tokkobroker.com/api/',
    'api_key' => '', //Api Key tokko env()

    //for matching TYPE on PROPERTIES
    'operations' => [
        'venta' => 1,
        'alquiler' => 2,
        'alquiler_temporario' => 3
    ],

    'default_filter_values' => [
        'price_from' => 0,
        'price_to' => 999999999999999,
        'currency' => 'ANY',
        'current_localization_id' => 0,
        'current_localization_type' => 'country',
        'operation_types' => [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],
        'property_types' => [1,2,3]
    ],

    'search_options' => [
        'operation_types' => [
            ['value' => 'en-venta', 'name' => 'Venta'],
            ['value' => 'en-alquiler', 'name' => 'Alquiler']
        ],
        'property_types' => [
            ['value' => 'terreno', 'name' => 'Terreno', 'comercial' => false, 'residencial' => false],
            ['value' => 'departamento', 'name' => 'Departamento', 'comercial' => false, 'residencial' => true],
            ['value' => 'casa', 'name' => 'Casa', 'comercial' => false, 'residencial' => true],
            ['value' => 'local', 'name' => 'Local', 'comercial' => true, 'residencial' => false],
            ['value' => 'deposito', 'name' => 'Depósito', 'comercial' => true, 'residencial' => false],
            ['value' => 'galpon', 'name' => 'Galpón', 'comercial' => true, 'residencial' => false],
            ['value' => 'oficina', 'name' => 'Oficina', 'comercial' => true, 'residencial' => false],
            ['value' => 'edificio-comercial', 'name' => 'Edificio Comercial', 'comercial' => true, 'residencial' => false],
        ],
        'room_amount' => [
            ['value' => '1-ambientes', 'name' => 'Monoambiente'],
            ['value' => '2-ambientes', 'name' => '2 Ambientes'],
            ['value' => '3-ambientes', 'name' => '3 Ambientes'],
            ['value' => '4-ambientes', 'name' => '4 Ambientes'],
            ['value' => '5-ambientes', 'name' => '5 Ambientes'],
            ['value' => '6-ambientes-o-mas', 'name' => '6+ Ambientes']
        ],
        'bathroom_amount' => [
            ['value' => '1-banios', 'name' => '1 Baño'],
            ['value' => '2-banios', 'name' => '2 Baños'],
            ['value' => '3-banios', 'name' => '3 Baños'],
            ['value' => '4-banios-o-mas', 'name' => '4+ Baños'],
        ],
        'currency' => [
            ['value' => 'ARS', 'name' => 'ARS'],
            ['value' => 'USD', 'name' => 'USD'],
        ],
    ],

    'custom_tags' => [
        'exclusivo' => 11274
    ],

    'preview_fields' => [
        'terreno' => [
            'Metros de frente',
            'Superficie vendible',
            'Frente',
        ]
    ],
    'detail_fields' => [
        'terreno' => [
            'Frente',
            'Fondo',
            'Morfología permitida',
            'Altura máxima',
            'Mixtura de usos',
            'Zonificación',
            'Superficie vendible',
            'Plusvalía (UVAs)',
        ]
    ],

    
    'agents' => [
        'Rodrigo' => [
            'name' => 'Guemes',
            'certificado'=> 'Martillero Publico',
            'matricula'=> 'Mat. CPMCLZ',
            'cargo'=>'Director'
        ],
        'Francisco' => [
            'name' => 'Lic. Francisco Rossi',
            'certificado'=> 'Martillero Publico',
            'matricula'=> 'Mat. CPMCLZ',
            'cargo'=>'Director'
        ],
        'juan' => [
            'name' => 'Juan José Gazda',
            'certificado'=> 'Martillero Publico',
            'matricula'=> 'Mat. CUCICBA',
            'cargo'=>'Director'
        ],
        'marcelo' => [
            'name' => 'Marcelo Bianco',
            'cargo'=>' Socio Gte Sucursal Canning',
            'certificado'=> null,
            'matricula'=> null
            
        ],
    ]
];
