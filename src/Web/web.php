<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('locations', 'LocationsController@index')->name('locations');
Route::get('developments/{id}', 'DevelopmentsController@show')->name('developments_show')->where('id', '[0-9]+');
Route::get('developments', 'DevelopmentsController@index')->name('developments');
Route::get('properties/{id}', 'PropertiesController@show')->name('properties_show')->where('id', '[0-9]+');
Route::get('properties', 'PropertiesController@index')->name('properties');



