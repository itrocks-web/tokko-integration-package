<?php

namespace App\Http\Controllers;

use App\Helpers\WebHelper;
use App\Http\Services\TokkoService;
use App\Location;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;


class PropertiesController
{
public function index(Request $request, $tipo, $url = ''){
        $ajax = $request->ajax();

        if($tipo != 'propiedades')
            $url = $tipo . '-' . $url;

        $filters = WebHelper::parseUrl($url);

        if($ajax){
            $limit = $request->input('limit');
            $offset = $request->input('offset');
        }else{
            $limit = 20;
            $offset = 0;
        }

        $tokkoService = new TokkoService();
        $results = $tokkoService->searchProperties($filters, $limit, $offset);

        WebHelper::buildSearchTitle($filters);

        $operation_type = null;

        if(count($filters['operation_types']) == 1){
            switch($filters['operation_types'][0]){
                case 1:
                    $operation_type = 'Venta';
                    break;
                case 2:
                    $operation_type = 'Alquiler';
                    break;
                case 3:
                    $operation_type = 'Alquiler temporario';
                    break;
                default:
                    break;
            }
        }

        if(!$ajax)
          $summary = $tokkoService->allSummary($filters);

        $data = [];

        $data['properties'] = $results->objects;
        
        $data['total_results'] = $results->meta->total_count;
        $data['operation_type'] = $operation_type;

        $data['applied_filters'] = $filters['applied'];
        $data['search_title'] = $filters['title'];

        $data['search_options'] = config('tokko.search_options');

        // $data['search_options']['locations'] = [];

        // $locations = Location::orderBy('name')->get();

        // foreach($locations as $location){
        //     $data['search_options']['locations'][] = [
        //         'value' => $location->getSearchFilterValue(),
        //         'name' => $location->name
        //     ];
        // }

        if(!$ajax){
            $data['summary'] = $summary->objects;
            $data['url'] = $url;
            $data['maps_api_key'] = env('MAPS_API_KEY');
        }else{
            foreach($data['properties'] as $k=>$property){
                $title_link = WebHelper::propertyUrl($property, $operation_type);

                $data['properties'][$k]->link = route('propertyDetail', ['id' => $property->id, 'url' => $title_link]);
                $data['properties'][$k]->front_cover = WebHelper::frontCoverPhoto($property);

                foreach($property->operations as $operation){
                    if($operation->operation_type == $operation_type)
                        $data['properties'][$k]->visible_price = $operation->prices[0]->currency . ' ' . number_format($operation->prices[0]->price, 0, ',', '.');
                }
            }
        }

        $data['filters'] = $filters;

        if($request->ajax()){
            $response = '';

            foreach($data['properties'] as $property){
                $response .= ( view('partial.property-card-search', [
                    'property' => $property,
                    'operation_type' => $operation_type
                ])->render() );
            }

            return response($response);
        }else{
            return view('properties', $data);
        }
    }

    public function show(Request $request, $id, $url = ''){
        $tokkoService = new TokkoService();
        $property = $tokkoService->property($id);

      
        if (isset($property->photos)) {
            $front_cover = url('assets/img/logogazda.jpg');
        }else{
        $front_cover = $property->photos[0];
        }
        foreach($property->photos as $photo){
            if($photo->is_front_cover){
                $front_cover = $photo;
            }
           ;
        }

        if(strpos($url, 'alquiler-temporario') !== FALSE)
            $operation_type = 'Alquiler temporario';
        elseif(strpos($url, 'alquiler') !== FALSE)
            $operation_type = 'Alquiler';
        elseif(strpos($url, 'venta') !== FALSE)
            $operation_type = 'Venta';
        else
            $operation_type = $property->operations[0]->operation_type;

        $filters = WebHelper::propertySimilarFilter($property, $operation_type);
        $similarProperties = $tokkoService->searchProperties($filters, 6);

        foreach($property->operations as $operations){
            if($operations->operation_type == $operation_type)
                $operation = $operations;
        }

        

        $data = [];

        $data['property'] = $property;
        $data['front_cover'] = $front_cover;
        $data['operation'] = $operation;
        $data['similarProperties'] = $similarProperties->objects;

        return view('property-detail', $data);
    }
    
    


}
