<?php

namespace App\Http\Controllers;

use App\Http\Services\TokkoService;
use Illuminate\Http\Request;

class DevelopmentsController extends Controller
{
    
    public function index(Request $request, $location = ''){
        $tokkoService = new TokkoService();
        $results = $tokkoService->searchDevelopments(99, 0);

        return view('developments', [
            'developments' => $results->objects,
            'location' => str_replace('en-', '', $location)
        ]);
    }

    public function show(Request $request, $id, $url = ''){
        $tokkoService = new TokkoService();
        $development = $tokkoService->development($id);

        foreach($development->photos as $photo){
            if($photo->is_front_cover)
                $front_cover = $photo;
        }

        $properties = $tokkoService->developmentProperties($id);

        $data = [];

        $data['development'] = $development;
        $data['front_cover'] = $front_cover;
        $data['properties'] = $properties->objects;
        $data['similarProperties'] = $properties->objects;

        return view('development-detail', $data);
    }
}
