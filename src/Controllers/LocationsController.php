<?php

namespace App\Http\Controllers;

use App\Http\Services\TokkoService;
use Illuminate\Http\Request;

class LocationsController extends Controller
{
    public function index(Request $request){
        $tokkoService = new TokkoService();

        $term = $request->input('text');

        $locations = $tokkoService->searchLocations($term);

        return response()->json($locations->objects);
    }
}